package Model;

import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import View.GameBoard;
import View.GUI;


import java.awt.Color;


// Speichern und laden von Spielständen

public class GameSaver {
	
	 private static final String PATH = "./data/";
	

	public static void saveGame(Spieler[][] board, Spieler[] spieler , Spieler currentPlayer)
	{
	
	try
	{
		
		File file = new File(PATH);
		file.mkdir();
		
        BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + "GameStatus.txt", false));
		
		//Speichern der Spieler
        
        for(Spieler s : spieler)
        {
            writer.write(s.getInt());
            writer.newLine();

         }
        
	}	
		
		
		

	}
	
}
