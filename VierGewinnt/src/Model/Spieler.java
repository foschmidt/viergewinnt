package Model;

public class Spieler
{
    private int spielerNum;
    public int wins = 0;

    /**
     * Spielerobjekt 
     * A player object to keep track of game players
     * playerNum = Spieler-ID
     */
    public Spieler(int spielerNum)
    {
        this.spielerNum = spielerNum;
    }

    /**
     * Getter der Spieler-ID
     *
     * R�ckgabewert entspricht Spieler-ID
     */
    public int getInt()
    {
        return spielerNum;
    }
    

    
    
}