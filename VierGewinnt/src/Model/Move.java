package Model;

public class Move
{
    private int position;

    /**
     * Erzeugen einer Wrapper-Klasse f�r die Position des Spielers
     *
     * position -  Spalte des Spielzugs
     */
    public Move(int position)
    {
        this.position = position;
    }

    /**
     * Wo finded der Spielzug statt?
     * R�ckgabewert entspricht der Position des Spielzuges
     */
    public int getPosition()
    {
        return position;
    }
}
