package Model;

import View.GameBoard;

public class SpielLogik
{
	//GameBoard beginnt bei Position 0,0 in der oberen linken Ecke
    private static final int TOP_ROW      = 0;
    private static final int FAR_LEFT_COL = 0;

    private static SpielLogik engine = null;
    private GameBoard board;
    private Spieler    s1, s2, currentSpieler;
    //Spalte die als letztes angeklickt wurde
    private Move lastColumnClicked = null;

    
    /**
     * Beschreibt die verschiedenen M�glichkeiten, wie das GameBoard (Array) von einer bestimmten Position aus durchsucht werden kann.
     * Jeder Wert stellt einen Schritt in eine Richtung dar, die man bei der Suche nach einer Richtung nutzen kann.
     * Somit wird die jeweilige Rechenoperation f�r currentPos += SearchDir.value durchgef�hrt (Addition / Subtraktion)
     */
    private enum SearchDir
    {
    	//GameBoard beginnt bei Position 0,0 in der oberen linken Ecke
        None(0), // keine Richtung wird angefasst
        Left(-1),
        Right(1),
        Down(1),
        Up(-1); //  Die oberste Spalte ist hierbei der letzte m�gliche Punkt einen Stein zu setzten
        private int value;

        SearchDir(int value) { this.value = value; }

        public int getValue() { return value; }
    }

    /**
	*s1 = Spieler 1
	*s2 = Spieler 2
	*/
    private SpielLogik(Spieler s1, Spieler s2)
    {
        board = GameBoard.getInstance();
        this.s1 = s1;
        this.s2 = s2;
        currentSpieler = this.s1;
    }

    /**
     * Singleton for SpielLogik. Gets an instance of the game engine or creates
     * a new one.
     *
     * @param s1 - Spieler 1
     * @param s2 - Spieler 2
     * @return a game instance
     */
    public static synchronized SpielLogik getInstance(Spieler s1, Spieler s2)
    {
        if (engine == null) {
            engine = new SpielLogik(s1, s2);
        }
        return engine;
    }

    /**
     * Setzt den Spielstein in die vom jeweiligen Spieler ausgesuchte Spalte
     * move = Position auf dem der Spielstein gesetzt werden soll
     * R�ckgabewert true wenn Position nicht vorherig belegt und Spielstein zur Position hinzugef�gt wurde
     * OustideBoardException, sofern Position ung�ltig
     */
    public boolean putDisc(Move move) throws OutsideBoardException
    {
        if (board.putDisc(currentSpieler.getInt(), move.getPosition())) {
            lastColumnClicked = move;
            nextTurn();
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Wechselt wer am Zug ist
     */
    private void nextTurn()
    {
        currentSpieler = currentSpieler == s1 ? s2 : s1;
    }

    /**
     * Spiellogik, die nach einem Gewinner in jedweder Richtung Ausschau h�llt (basierend auf der als letztes angeklickten Spalte)
     * R�ckgabe von true wenn Gewinner gefunden wurde
     */
    private boolean declareWinner()
    {
        try {
        	//sucht horizontal nach einem Gewinner
            int rowPieces = findWinner(SearchDir.Right, SearchDir.None) + findWinner(SearchDir.Left, SearchDir.None) - 1;
            if (rowPieces >= 4) {
                return true;
            }
            // sucht vertikal nach einem Gewinner
            int columnPieces = findWinner(SearchDir.None, SearchDir.Down) + findWinner(SearchDir.None, SearchDir.Up) - 1;
            if (columnPieces >= 4) {
                return true;
            }
            //sucht diagnoal links nach rechts nach einem Gewinner
            int backSlashPieces = findWinner(SearchDir.Right, SearchDir.Down) + findWinner(SearchDir.Left, SearchDir.Up) - 1;
            if (backSlashPieces >= 4) {
                return true;
            }
            // sucht diagonal von rechts nach links nach einem Gewinner
            int foreSlashPieces = findWinner(SearchDir.Right, SearchDir.Up) + findWinner(SearchDir.Left, SearchDir.Down) - 1;
            if (foreSlashPieces >= 4) {
                return true;
            }
        } catch (OutsideBoardException ignored) {

        }
        return false;
    }

    /**
     * Sucht nach einem Gewinner in jeder erforderlichen Richtung
     * horzSearchDir = aktuelle horizontale Suchrichtung (links, rechts oder keine)
     * vertSearchDir = aktuelle vertikale Suchrichtung (hoch, runter oder keine)
     * R�ckgabewert enspricht dem Wert der verbundenen Spielsteine eines Spielern in eine Richtung 
     * OutsideBoardException sofern die Suche das GameBoard verl�sst
     */
    private int findWinner(SearchDir horzSearchDir, SearchDir vertSearchDir) throws OutsideBoardException
    {
        int lastMoveColumn = lastColumnClicked.getPosition();
        int lastMoveRow = board.getRowNumber() - board.getColumnHeight(lastMoveColumn);

        int curRow = lastMoveRow;
        int curCol = lastMoveColumn;
        int count = 0;

        do {
            curRow += vertSearchDir.getValue();
            curCol += horzSearchDir.getValue();
            ++count;
        }
        while (count < 4 && !outOfBounds(curRow, curCol) && samePosition(lastMoveColumn, lastMoveRow, curRow, curCol));
        return count;
    }

    /**
     * Abfrage ob aktuelle Position sich auf dem GameBoard befindet
     * curRow = fragliche Zeile
	 * curCol - fragliche Spalte
	 * R�ckgabewert true sofern Position au�erhalb der GameBoardgrenzen liegt
     */
    private boolean outOfBounds(int curRow, int curCol)
    {
        return curRow < TOP_ROW || pastLastRow(curRow) || curCol < FAR_LEFT_COL || pastLastColumn(curCol);
    }

    /**
     * Abgleich ob aktuelle Position die als letztes angeklickte Position des letzten Spielers ist
     * lastMoveColumn = zuletzt angeklickte Spaltenposition
     * lastMoveRow = zuletzt angeklickte Reihenposition
     * curRow = aktuell durchsuchte Reihe
     * curCol = aktuiell durchsuchte Zeile
     * R�ckgabewert true sofern sicht die letzte und aktuelle Position �berschneiden
     */
    private boolean samePosition(int lastMoveColumn, int lastMoveRow, int curRow, int curCol) throws OutsideBoardException
    {
        return board.getBoardPos(curRow, curCol) == board.getBoardPos(lastMoveRow, lastMoveColumn);
    }

    /**
     * Ist die aktuelle Position am �u�ersten rechten Rand des GameBoards?
     * curCol - fragliche Reihe
     * R�ckgabewert true wenn es die letzte Reihe (am weitesten rechts ist)
     */
    private boolean pastLastColumn(int curCol)
    {
        return curCol == board.getColumnNumber();
    }

    /**
     * Ist die aktuelle Position die innerhalb der obersten Zeile?
     *
     * curRow - fragliche Zeile
     * R�ckgabewert true sofern es dioe oberste Reihe ist
     */
    private boolean pastLastRow(int curRow)
    {
        return curRow == board.getRowNumber();
    }

    /**
     * Ist das Spiel vorbei (GameBoard voll, Gewinner gefunden)?
     * Gibt den Gewinner zur�ck, sofern es einen gibt. Bei unentschieden wird Spieler 0 zur�ckghegeben.
     * Dauert das Spiel noch an wird null zur�ckgegeben.
     */
    public Spieler isGameOver()
    {
        if (board.isBoardFull()) {
            return new Spieler(0);
        }
        if (declareWinner()) {

            if (currentSpieler == s1) {
                ++s2.wins;
                return s2;
            }
            else {
                ++s1.wins;
                return s1;
            }
        }
        return null;
    }

    /**
     * Spielstand
     * R�ckgabewert entspricht Anzahl der gewonnenen Spiele je Spieler
     */
    public int[] getScore()
    {
        return new int[] {s1.wins, s2.wins};
    }

    /**
     * Aktueller Spieler
     */
    public Spieler getAktuellenSpieler()
    {
        return currentSpieler;
    }

    /**
     * Gesamtanzahl der Zeilen des GameBoards
     * R�ckgabewert entspricht gesamten Zeilenl�nge
     */
    public int getRowNumber()
    {
        return board.getRowNumber();
    }

    /**
     * Gesamtanzahl der Spalten des GameBoards
     * R�ckgabewert entspricht gesamten Spaltenl�nge
     */
    public int getColumnNumber()
    {
        return board.getColumnNumber();
    }

    public void clearBoard()
    {
        board.clearBoard();
    }

    /**
     * Gibt die derzeige GameBoardinstanz zur�ck
     * Return an instance of the current game board
     */
    public GameBoard getBoard()
    {
        return board;
    }
}
