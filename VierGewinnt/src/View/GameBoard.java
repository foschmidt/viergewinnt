package View;

import Model.OutsideBoardException;

public class GameBoard
{
    /**
     * Position "leer"
     */
    private static final int       EMPTY_POS = 0;
    private static       GameBoard GameBoard = null;


   //  Spieler 1, 2 oder niemand (0) kontrolliert eine Position

    int[][] board;

    /**
     * Dimension des GameBoardes
     */
    private static final int ROWS    = 7;
    private static final int COLOMN = 7;


    private GameBoard()
    {
        clearBoard();
    }



     //  @return Erstellt neue Instanz eines GameBoardes 

    public static synchronized GameBoard getInstance()
    {
        if (GameBoard == null) {
            GameBoard = new GameBoard();
        }
        return GameBoard;
    }

    /**
     * Abfrage ob Position besetzt ist
     * row beschreibt hierbei den Index der Zeile.
     * column beschreibt den Index der Spalte.
     * R�ckgabe von true wenn Position bereits besetzt ist.
     */
    public boolean fullAtPos(int row, int column) throws OutsideBoardException
    {
        if (isValidColumn(column) && isValidRow(row)) {
            return board[row][column] != EMPTY_POS;
        }
        else {
            throw new OutsideBoardException();
        }
    }

    /**
     * Abfrage: Ist das GameBoard vollst�ndig gef�llt?
     * R�ckgabe von true wenn GameBoard vollst�ndig gef�llt ist und somit keine leere
     * Position mehr vorhanden ist.
	*/
    public boolean isBoardFull()
    {
        boolean b = true;
        for (int i = 0; i < COLOMN; i++) {
            try {
                b &= fullAtPos(0, i);
            } catch (OutsideBoardException ignored) {
            }
        }
        return b;
    }

    /**
     * Setzt den Spielstein des jeweiligen Spielers in die unterste freie Position der Spalte (column)
     *
     * playerNumber beschreibt hierbei die Spielernummer
     * columnNumber - the column index to put the player/disc in it.
     * Gibt den Wert true zur�ck, wenn Spielstein (disc) hinzugef�gt wurde, false sofern die Spaltenposition voll ist.
     * OutsideBoardException sofern die Position ung�ltig ist.

     */
    public boolean putDisc(int playerNumber, int columnNumber) throws OutsideBoardException
    {
        if (isValidColumn(columnNumber)) {
            for (int i = ROWS - 1; i >= 0; i--) {
                if (board[i][columnNumber] == EMPTY_POS) {
                    board[i][columnNumber] = playerNumber;
                    return true;
                }
            }
            return false;
        }
        throw new OutsideBoardException();
    }

    /**
     * Getter f�r Position auf dem GameBoard
     * rowIndex beschreibt hierbei die Position der Zeile (row)
     * columnIndex beschreibt hierbei die Position der Spalte (column)
     * Gibt die Spielernummer (disc) f�r die gew�nschte Position aus
     * OutsideBoardException sofern die Position ung�ltig ist
     */
    public int getBoardPos(int rowIndex, int columnIndex) throws OutsideBoardException
    {
        if (isValidColumn(columnIndex) && isValidRow(rowIndex)) {
            return board[rowIndex][columnIndex];
        }
        throw new OutsideBoardException();
    }

    /**
     * Getter f�r die Anzahl der gef�llten Positionen innerhalb einer Spalte
     * columnIndex beschreibt hierbei die erfragte Spalte
     * R�ckgabewert entspricht der Anzhal ausgef�llter Positionen
     * OutsideBoardException sofern die Position ung�ltig ist
     */
    public int getColumnHeight(int columnIndex) throws OutsideBoardException
    {
        if (!isValidColumn(columnIndex)) {
            throw new OutsideBoardException();
        }
        int i = 0;
        while (i < ROWS && board[i][columnIndex] == EMPTY_POS) {
            ++i;
        }
        return ROWS - i;
    }

    /**
     * Pr�ft die �bergebene Spaltennummer (index)  auf G�ltigkeit
     * columnNumber ist hierbei der zu pr�fende Spaltenindex
     * R�ckgabewert true wenn Spalte (column) auf den GameBoard vorhanden ist
     */
    public boolean isValidColumn(int columnNumber)
    {
        return columnNumber >= 0 && columnNumber <= this.COLOMN - 1;
    }

    /**
     * Pr�ft die �bergebene Zeilennummer (index)  auf G�ltigkeit
     * rowNumber ist hierbei der zu pr�fende Zeilenindex
     * R�ckgabewert true wenn Zeile (row) auf den GameBoard vorhanden ist
     */

    public boolean isValidRow(int rowNumber)
    {
        return rowNumber >= 0 && rowNumber <= this.ROWS - 1;
    }

    /**
     * GameBoard mit Hilfe eines Array dargestellt
     * R�ckgabewert ist das GameBoard als 2d Array
     */
    public int[][] getBoardArray()
    {
        return board;
    }

    public final void clearBoard()
    {
        board = new int[ROWS][COLOMN];
    }

    /**
     * Getter f�r die Anzahl der Zeilen des GameBoardes
     * R�ckgabewert entspricht Anzahl der Zeilen
     */
    public int getRowNumber()
    {
        return ROWS;
    }

    /**
     * Getter f�r die Anzahl der Spalten des GameBoardes
     * R�ckgabewert entspricht Anzahl der Spalten
     */
    public int getColumnNumber()
    {
        return COLOMN;
    }
}