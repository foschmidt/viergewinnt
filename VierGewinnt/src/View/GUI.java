package View;

import javax.swing.*;

import Model.SpielLogik;
import Model.Move;
import Model.OutsideBoardException;
import Model.Spieler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GUI extends MouseAdapter
{
    /**
     * Icons der Spielsteine
     */
    private static final String ICON_LEER = "VierGewinnt/src/img/leer_01.png";
    private static final String ICON_ROT   = "VierGewinnt/src/img/rot_01.png";
    private static final String ICON_GELB = "VierGewinnt/src/img/gelb.png";

    private static final int        IMG_SIZE = 50;
    private              SpielLogik engine   = null;
    private JFrame frame;

    /**
     * board wird als Array dargestellt
     * Jede Position wird durch ein Bild bef�llt
     */
    private JLabel[][] board = null;
    private JLabel score;
    private JLabel currentTurn = null;
    private JMenuBar menuBar;

    /**
     * M�gliche Werte f�r einen Spielstein, der in eine Spalte gesetzt wird
     * Default nimmt an, Spalte sei leer
     */
    private enum Disc
    {
        None,
        Spieler1,
        Spieler2
    }

    /**
     * Fenstererstellung (GUI)
     */
    public GUI(Spieler p1, Spieler p2)
    {
        frame = new JFrame();
        frame.setTitle("Vier Gewinnt!");
        score = new JLabel();
        menuBar = new JMenuBar();
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        engine = SpielLogik.getInstance(p1, p2);
        createMenu();
    }

    /**
     * Spiel wird in festgelegter Gr��e gestartet
     * listener wird hinzugef�gt um Input sicherzustellen

     */
    public void startGame()
    {
        initboard();
        frame.setSize(800, IMG_SIZE * 9);
        frame.addMouseListener(this);
    }

    /**
     * Obere Men�leiste
     */
    public final void createMenu()
    {
        JMenu file = new JMenu("Spiel");
        JMenuItem exitItem = new JMenuItem("Verlassen");
        exitItem.addActionListener(new ExitApp(frame));
        file.add(exitItem);
        menuBar.add(file);
        JMenu help = new JMenu("Hilfe");
        JMenuItem helpItem = new JMenuItem("Anleitung");
        helpItem.addActionListener(new HelpMenu(frame));
        helpItem.setSize(300, 200);
        help.add(helpItem);
        menuBar.add(help);
        frame.setJMenuBar(menuBar);
    }

    /**
     * boardaktualisierung
     * Austausch des alten Bildes (leer = wei�) mit dem jeweilig neuen des Spielers (schwarz / rot) um Spielstein zu "setzen"
     */
    public void updateboard()
    {
        int[][] a = engine.getBoard().getBoardArray();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                Disc pos = Disc.values()[a[i][j]];
                switch (pos) {
                    case None:
                        board[i][j].setIcon(new ImageIcon(ICON_LEER ));
                        break;
                    case Spieler1:
                        board[i][j].setIcon(new ImageIcon(ICON_ROT));
                        break;
                    case Spieler2:
                        board[i][j].setIcon(new ImageIcon(ICON_GELB));
                        break;
                }
            }
        }
    }

    /**
     * Wird aufgerufen sobald das Spiel vorr�ber ist.
     * Gibt den Spielern zwei Entscheidungen
     * 1. Erneut spielen
     * 2. Spiel verlassen
     * Gibt ebenfalls den Gewinner bekannt, sofern es einen gibt
     */
    public boolean gameOver(Spieler winner)
    {
        String message = "Game Over, Draw.";
        if (winner.getInt() != 0) {
            message = String.format("Spiel vorbei, Spieler %d gewinnt.", winner.getInt());
        }
        int playAgain = JOptionPane.showConfirmDialog(frame, message, "Spiel vorbei, neue Runde?", JOptionPane.YES_NO_OPTION);

        if (playAgain == JOptionPane.YES_OPTION) {
            initboard();
            updateboard();

            return false;
        }
        return true;
    }

    public void updateScoreText()
    {
        int[] SpielerScores = engine.getScore();
        score.setText(String.format("Spielstand: %s - %s", SpielerScores[0], SpielerScores[1]));
    }

    public void updateTurnText(Spieler currentSpieler)
    {
        currentSpieler = currentSpieler == null ? new Spieler(1) : currentSpieler;
        String color = currentSpieler.getInt() == 1 ? "Rot" : "Gelb";
        currentTurn.setText(String.format("Spieler %s (%s) an der Reihe!", currentSpieler.getInt(), color));
    }

    /**
     * F�gt den Spielstein des jeweiligen Spielers dem board hinzu

     * columnNumber = Spalte in der der Spielstein hinzugef�gt werden soll
     * R�ckgabewert true wenn der Spielstein erfolgreich hinzugef�gt werden konnte (Spalte nicht voll)
     */
    public boolean putDisc(int columnNumber) throws OutsideBoardException
    {
        boolean putIsDone = engine.putDisc(new Move(columnNumber));
        updateTurnText(engine.getAktuellenSpieler());

        if (putIsDone) {
            Spieler p = engine.isGameOver();
            updateboard();
            updateScoreText();
            if (p != null) {
                boolean noMoreGames = gameOver(p);

                if (noMoreGames) {
                    frame.removeMouseListener(this);
                }
            }
        }
        return putIsDone;
    }

    /**
     * Stellt fest an welcher Stelle der Mausklick erfolgte
     * Weist Spielstein der geklickten Spalte zu
     */
    @Override
    public void mousePressed(MouseEvent mouseEvent)
    {
        try {
            if (mouseEvent.getY() < IMG_SIZE * (engine.getRowNumber() + 0.5f) &&
                mouseEvent.getX() < IMG_SIZE * engine.getColumnNumber()) {
                putDisc(mouseEvent.getX() / IMG_SIZE);
            }
        } catch (OutsideBoardException ignored) {
        }
    }


    //Initialisiert ein neues board und wei�t jeder Position einen leeren (wei�) Platz zu


    private void initboard()
    {
        engine.clearBoard();
        frame.getContentPane().removeAll();
        board = new JLabel[engine.getRowNumber()][engine.getColumnNumber()];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                board[i][j] = new JLabel();
                board[i][j].setBounds(j * IMG_SIZE, i * IMG_SIZE, IMG_SIZE, IMG_SIZE);
                board[i][j].setIcon(new ImageIcon(ICON_LEER));
                frame.getContentPane().add(board[i][j]);
            }
        }
        //�bersicht �ber Spielstand und welcher Spieler an der Reihe ist zu setzten
        currentTurn = new JLabel();
        currentTurn.setBounds(8 * IMG_SIZE, 0, 200, 20);
        score.setBounds(12 * IMG_SIZE, 0, 200, 20);
        updateScoreText();
        updateTurnText(null);
        frame.getContentPane().add(currentTurn);
        frame.getContentPane().add(score);
        frame.setVisible(true);
    }


    //F�gt Kontextmen� f�r das Beenden der Anwendung hinzu


    private static class ExitApp implements ActionListener
    {
        private JFrame frame;

        ExitApp(JFrame frame)
        {
            this.frame = frame;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            int playAgain = JOptionPane.showConfirmDialog(frame, "M�chten Sie das Spiel wirklich beenden??", "", JOptionPane.YES_NO_OPTION);

            if (playAgain == JOptionPane.YES_OPTION) {
                System.exit(0);
            }
        }
    }

    /**
     * Kontextmen� f�r die Anleitung
     */
    private static class HelpMenu implements ActionListener
    {
        private JFrame frame;

        HelpMenu(JFrame frame)
        {
            this.frame = frame;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            String msg = "Setzt abwechselnd Spielsteine in die Reihen. \n" +
                "Ziel ist es, vier gleichfarbige Spielsteine in eine Reihe zu bringen (horizontal, vertikal oder diagonal). \n" +
                "Der erste, der vier Spielsteine in einer Reihe verbunden hat, gewinnt.\n";

            JOptionPane.showMessageDialog(frame, msg, "Spielanleitung", JOptionPane.OK_OPTION);
        }
    }
}
