package Main;

import Model.Spieler;
import View.GUI;

public class Main
{
    public static void main(String... args)
    {
        Spieler s1 = new Spieler(1);
        Spieler s2 = new Spieler(2);
        GUI ui = new GUI(s1, s2);
        ui.startGame();
    }
}